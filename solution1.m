function [forwBackVel, leftRightVel, rotVel, finish] = ...
solution1(pts, contacts, position, orientation, varargin)
% The control loop callback function - the solution 
% for task 1

    % get the destination point
    if length(varargin) ~= 3,
        error('Wrong number of additional arguments: %d\n', ...
            length(varargin));
    end
    dest_x = varargin{1};
    dest_y = varargin{2};
    dest_phi = varargin{3};

    % declare the persistent variable that keeps the 
    % state of the Finite
    % State Machine (FSM)
    persistent state;
    if isempty(state),
        % the initial state of the FSM is 'init'
        state = 'init';
    end
    
    % initialize the robot control variables 
    % (returned by this function)
    finish = false;
    forwBackVel = 0;
    leftRightVel = 0;
    rotVel = 0;
    % TODO: manage the states of FSM
    % Write your code here...

    persistent init_orient;
    
    u_max = 4;
    u_min = -4;
    gain_P = 50;

    if strcmp(state, 'init')
        state = 'move_to_dest';
        fprintf('changing FSM state to %s\n', state);
        init_orient=orientation(3);
    elseif strcmp(state, 'move_to_dest')
        position_diff=[(dest_x - position(1));
                        (dest_y - position(2))];
        distance=norm(position_diff);
        v=position_diff./distance;
        vel_theta = dest_phi/ distance;
        u_pos = gain_P * distance;
        if u_pos > u_max 
            u_pos = u_max;
        elseif u_pos < u_min
            u_pos = u_min;
        end
        v=v*u_pos;
        Vel_glob = [v(2); v(1); vel_theta];
        R=[cos(orientation(3)) -sin(orientation(3)) 0; 
            sin(orientation(3)) cos(orientation(3)) 0; 
            0 0 1];
        vel_local = R*Vel_glob;
        if abs(position(1) - dest_x) > 0.05 ...
            && abs(position(2) - dest_y) > 0.05
            forwBackVel = vel_local(1);
            leftRightVel = vel_local(2);
        end
        if abs(init_orient - orientation(3)) < ...
            abs(dest_phi - 0.0873)
            rotVel = vel_local(3);
        end
        if abs(init_orient - orientation(3)) > ...
                abs(dest_phi - 0.0873) ...
            && (abs(position(1) - dest_x) < 0.05 ...
            || abs(position(2) - dest_y) < 0.05)
            state = 'finish';
            fprintf('changing FSM state to %s\n', state);
        end    
    elseif strcmp(state, 'finish')
        finish = true;
        disp('finished');
    else
        error('Unknown state %s.\n', state);
    end
end